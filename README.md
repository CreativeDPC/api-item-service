
## Contácto

Daniel Pérez Cabrera    
Cel: 55 1120 0160    
Email:  creativedesing.dpc@gmail.com

## Descripción

API Rest que consume servicios de API Mercado Libre.    


## Instalación

```bash
$ yarn install
```

## Ejecución

```bash
# Desarrollo
$ yarn run start

# Modo Watch
$ yarn run start:dev

```

## End points

```bash
# Swagger
/api

# Obtiene una lista de items que estan relacionados con la cadena de texto enviada.
/api/items?q=:query

# Obtiene la especificación de un item por su ID.
/api/items/:id

```

## Tecnologías Utilizadas

- [Nest Js](https://nestjs.com/) (Javascript)

- [Mercado Libre API Developer](https://developers.mercadolibre.com.mx/)

## Prueba en vivo

[API Item Service by Daniel Pérez](https://item-service.herokuapp.com/api/)

## Imagenes

![Swagger](https://bitbucket.org/CreativeDPC/api-item-service/raw/3ff69e94622b9c9eb9e9988c011fcbf71c4d37a4/pictures/swagger-item-service.png)

