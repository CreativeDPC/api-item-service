import { ItemResultDto } from './dto/item-result.dto';
import { QueryResultDto } from './dto/query-result.dto';
import { Controller, Get, Param, Query, HttpException, NotFoundException, InternalServerErrorException,  } from '@nestjs/common';
import { ItemsService } from './items.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('Items')
@Controller('api/items')
export class ItemsController {
  constructor(private readonly itemsService: ItemsService) {}

  @ApiOperation({ summary: 'Obtiene una lista de items que estan relacionados con la cadena de texto enviada.' })
  @Get()
  async searchByQuery(@Query('q') query: string) : Promise<QueryResultDto | HttpException>  {
    try {
      return await this.itemsService.findByQuery(query);
    } catch (error) {
      return new InternalServerErrorException(error);      
    }
  }

  @ApiOperation({ summary: 'Obtiene la especificación de un item por su ID.' })
  @Get(':id')
  async searchById(@Param('id') id: string) : Promise<ItemResultDto | HttpException> {
    try {
      return await this.itemsService.findById(id);
    } catch (error) {
      return new InternalServerErrorException(error);
    }
    
  }

}
