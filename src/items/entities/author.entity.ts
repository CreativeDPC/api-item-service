export class Author{
    name:string;
    lastname:string;

    constructor(data:any = {}){
        this.name = data.name || "";
        this.lastname = data.lastname || "";
    }
}