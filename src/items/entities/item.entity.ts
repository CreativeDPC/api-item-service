import { Price } from "./price.entity";

export class Item {
    id: string;
    title: string;
    price: Price; 
    picture: string;
    condition: string;
    free_shipping: boolean;
    sold_quantity?: number;
    description?: string;
    paths?:Array<string>;

    constructor(data:any = {}){
        this.id = data.id || null;
        this.title = data.title || "";
        this.price = data.price || new Price();
        this.picture = data.picture || "";
        this.condition = data.condition || "";
        this.free_shipping = data.free_shipping || false;
        this.sold_quantity = data.sold_quantity;
        this.description = data.description;
        this.paths = data.paths;
    }

}

