export class Price {
    currency: string;
    amount: number;
    decimals: number;

    constructor(data:Price = undefined){
        this.currency = data.currency || "";
        this.amount = data.amount || null;
        this.decimals = data.decimals || null;
    }
}