import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class UtilService {

  URL_API:string;
  ID_SITE:string; 

  constructor(
    private httpService: HttpService,
    private configService: ConfigService
  ){
    this.URL_API = this.configService.get<string>('URL_API_ML'); 
    this.ID_SITE = this.configService.get<string>('SITE_ID'); 
  }

  /**
   * Método que obtiene el detalle de una divisa específica
   * @param currency_id Id de la Divisa
   */
  async GetCurrencyDetails(currency_id:string):Promise<any>{
    let currency_detail = {}; 
    await this.httpService.get(`${this.URL_API}/currencies/${currency_id}`).toPromise()
    .then( ({ data }) => {
      if(data){
        currency_detail = data;
      }
    })
    .catch(err => {
      throw Error(`COD: US01 - Error inesperado al conectar con la fuente de información. Detalle: ${err}`) 
    });
    
    return currency_detail;
  } 

  /**
   * Método que devuelve el detalle de un categoría específica
   * @param category_id Id de la Categoría
   */
  async GetCategoryDetails(category_id:string):Promise<any>{
    let category_detail = {}; 
    await this.httpService.get(`${this.URL_API}/categories/${category_id}`).toPromise()
    .then( ({ data }) => {
      if(data){
        category_detail = data;
      }
    })
    .catch(err => {
      throw Error(`COD: US02 - Error inesperado al conectar con la fuente de información. Detalle: ${err}`) 
    });
        
    return category_detail;
  }   

  /**
   * Método que devuelve la descripción de un Item
   * @param id_item Id del Item
   */
  async GetItemDescription(id_item:string):Promise<any>{
    return this.httpService.get(`${this.URL_API}/items/${id_item}/description`).toPromise();
  }  

}
