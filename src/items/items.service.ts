import { UtilService } from './services/util.service';
import { Item } from './entities/item.entity';

import { ItemResultDto } from './dto/item-result.dto';
import { QueryResultDto } from './dto/query-result.dto';

import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Price } from './entities/price.entity';


@Injectable()
export class ItemsService {

  URL_API:string;
  ID_SITE:string; 

  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private utilService: UtilService
  ){
    this.URL_API = this.configService.get<string>('URL_API_ML'); 
    this.ID_SITE = this.configService.get<string>('SITE_ID'); 
  }

  /**
   * Método que devuelve una lista de Items relacionados con la cadena de busqueda 
   * @param query Cadena de texto con la que se realizara la búsqueda
   */
  async findByQuery(query:string): Promise<QueryResultDto> {
    let queryResult:QueryResultDto = new QueryResultDto();

    await this.httpService.get(`${this.URL_API}/sites/${this.ID_SITE}/search?q=${query}&limit=4`).toPromise()
    .then( async ({ data }) => {
      
      if(data && data.results){
        const {results, filters} = data;
       
        queryResult.items =  await Promise.all(await this.ConvertResultToArrayItems(results));
        queryResult.categories =  this.GetPathFromRootByFilter(filters); //await Promise.all(await this.GetNameCategoriesByCategoryId(results));
      }

    })
    .catch(err => {
      throw Error(`COD: IS01 - Error inesperado al conectar con la fuente de información. Detalle: ${err}`) 
    });

    return queryResult;
  }

  /**
   *  Método que devuelve la inforación de un Item en especifico
   * @param id Id del Item 
   */
  async findById(id: string): Promise<ItemResultDto> {
    let itemResultDto:ItemResultDto = new ItemResultDto();
    let categoryId="";

    let _item:Item = await this.httpService.get(`${this.URL_API}/items/${id}`).toPromise()
    .then( ({ data }) => {
      
      if(data){
        const { id, title, price, pictures, condition, shipping, currency_id, sold_quantity, category_id } = data;
        const [image] = pictures;

        categoryId = category_id;

        return new Item({
          id,
          title,
          price:new Price({
            amount: price,
            currency: currency_id,
            decimals: null
          }),
          picture: image?.url,
          condition,
          free_shipping: shipping?.free_shipping,
          sold_quantity,
          description: ""
        })
      }else{
        return new Item()
      }
    })
    .catch(err => {
      throw Error(`COD: IS02 - Error inesperado al conectar con la fuente de información. Detalle: ${err}`) 
    });

    if(_item.id){
      const { decimal_places } = await this.utilService.GetCurrencyDetails(_item.price.currency);
      const {data:{plain_text}} = await this.utilService.GetItemDescription(id);
      const {path_from_root} = await this.utilService.GetCategoryDetails(categoryId);
      
      _item.price.decimals = decimal_places;
      _item.description = plain_text;
      
      if(path_from_root){
        _item.paths = [...path_from_root.map(p => p.name)];
      }
    }
    
    itemResultDto.item = _item;
    
    return itemResultDto;
  }

  //#region Métodos Privados  

  private async ConvertResultToArrayItems(results){

    return await results.map( async (_item) => {
      const { id, title, price, thumbnail, condition, shipping, currency_id } = _item;
      const { decimal_places } = await this.utilService.GetCurrencyDetails(currency_id);
      const {data:{pictures}} =  await this.httpService.get(`${this.URL_API}/items/${id}`).toPromise();
      const [picture] = pictures;

      return new Item({
        id,
        title,
        price:new Price({
          amount: price,
          currency: currency_id,
          decimals: decimal_places
        }),
        picture: picture.url,
        condition,
        free_shipping: shipping?.free_shipping,
      })
    });

  }

  private GetPathFromRootByFilter(filters){
    
    let findCategoryFilters = filters.find(f => f.id == "category");
    let paths = [];
    
    if(findCategoryFilters){
      const {values} = findCategoryFilters;
      const [category] = values;

      paths = [...category.path_from_root.map(c => c.name)]; 
    }
    
     return paths;
  }

  //#endregion

}
