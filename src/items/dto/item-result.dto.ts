import { Item } from '../entities/item.entity';
import { Author } from '../entities/author.entity';

export class ItemResultDto {
    author:Author = new Author();
    item?: Item;
}
