import { Item } from './../entities/item.entity';
import { ItemResultDto } from './item-result.dto';
import { PartialType } from '@nestjs/mapped-types';

export class QueryResultDto extends PartialType(ItemResultDto) {
    items?:Array<Item>;
    categories:Array<String> = new Array<String>();
}
