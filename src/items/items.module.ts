import { UtilService } from './services/util.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HttpModule, Module } from '@nestjs/common';
import { ItemsService } from './items.service';
import { ItemsController } from './items.controller';

@Module({
  imports:[
    HttpModule
  ],
  controllers: [ItemsController],
  providers: [
    ConfigService,
    ItemsService, 
    UtilService
  ]
})
export class ItemsModule {}
